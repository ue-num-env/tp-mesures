import numpy as np
import matplotlib.pyplot as plt
import sys
import time

def simd_packet(c0, c1, c2, c3):
    return c0
def simd_all(x):
    return x
def simd_any(x):
    return x
def simd_range(x):
    return range(x)

from numba import jit,prange

@jit(nopython=True,parallel=True)
def mandelbrot(m, n, x0, y0, x1, y1, max_iter):
    
    # fonction calculant la suite pour le point c du plan complex
    # et retournant le nombre d'itérations à la détection de la divergence
    def kernel_simd(c0, c1, c2, c3, max_it):
        c = simd_packet(c0, c1, c2, c3)
        z = c
        i = simd_packet(0,0,0,0)
        while simd_all(i<max_it) and simd_any(abs(z) < 2):
            z = z**2 + c # operations vectorielles sur des packets SIMD
            i += 1 if abs(z) < 2 else 0
            if simd_all(abs(z) > 2):
                break
        return i

    X = np.linspace(x0, x1, n)
    Y = np.linspace(y0, y1, m)

    img = np.zeros((m,n))
    # for each row of the image
    for i in prange(0,m):
        for j in range(0,m,4):
            img[i:i+3] = kernel_simd(X[j] + 1j*Y[i], X[j+1] + 1j*Y[i], X[j+2] + 1j*Y[i], X[j+3] + 1j*Y[i], max_iter)
    return img

def main():
    n = 256; # image size
    t0 = time.time()
    img = mandelbrot(n, n, 0.273771332381423218946, 0.595859541361479164066,
                           0.273771332946091993361, 0.595859541784980744876, 10000)
    t1 = time.time()
    print("Running time: ", t1-t0, "s")
    print("Saving file...")
    plt.imsave('mandelbrot.jpg', img)
    return 0

if __name__ == '__main__':
    sys.exit(main())
