import numpy as np
import matplotlib.pyplot as plt
import sys
import time
from numba import jit,prange

@jit(nopython=True,parallel=True)
def mandelbrot(m, n, x0, y0, x1, y1, max_iter):
    
    # fonction calculant la suite pour le point c du plan complex
    # et retournant le nombre d'itérations à la détection de la divergence
    def kernel_pipe(c0, c1, c2, c3, max_it):
        z0,z1,z2,z3 = c0,c1,c2,c3
        i0 = i1 = i2 = i3 = 0
        while (i0<max_it and i1<max_it and i2<max_it and i3<max_it):
            z0 = z0**2 + c0
            z1 = z1**2 + c1
            z2 = z2**2 + c2
            z3 = z3**2 + c3
            a0, a1, a2, a3 = abs(z0), abs(z1), abs(z2), abs(z3)
            i0 += 1 if a0 < 2 else 0
            i1 += 1 if a1 < 2 else 0
            i2 += 1 if a2 < 2 else 0
            i3 += 1 if a3 < 2 else 0
            if not (a0<2 or a1<2 or a2<2 or a3<2):
                break
        return np.array([i0, i1, i2, i3])

    X = np.linspace(x0, x1, n)
    Y = np.linspace(y0, y1, m)

    img = np.zeros((m,n))
    # for each row of the image
    for i in prange(0,m):
        for j in range(0,m,4):
            img[i,j:j+4] = kernel_pipe(X[j] + 1j*Y[i], X[j+1] + 1j*Y[i], X[j+2] + 1j*Y[i], X[j+3] + 1j*Y[i], max_iter)
    return img

def main():
    n = 2048; # image size
    t0 = time.time()
    img = mandelbrot(n, n, 0.273771332381423218946, 0.595859541361479164066, 0.273771332946091993361, 0.595859541784980744876, 10000)
    t1 = time.time()
    print("Running time: ", t1-t0, "s")
    print("Saving file...")
    plt.imsave('mandelbrot.jpg', img)
    return 0

if __name__ == '__main__':
    sys.exit(main())