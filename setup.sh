#!/bin/bash

mkdir -p powermeter/build
cd powermeter/build
cmake .. -DCMAKE_BUILD_TYPE=Release
make -j 9
cd -
