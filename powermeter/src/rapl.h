#pragma once

#include <cstdint>

class Domain;
struct pkg_energy_statistics_t;

class RAPL
{

#if defined(__APPLE__)
  bool mIsGpuSupported;  // Is the GPU domain supported by the processor?
  bool mIsRamSupported;  // Is the RAM domain supported by the processor?

  // The DRAM domain on Haswell servers has a fixed energy unit (1/65536 J ==
  // 15.3 microJoules) which is different to the power unit MSR. (See the
  // "Intel Xeon Processor E5-1600 and E5-2600 v3 Product Families, Volume 2 of
  // 2, Registers" datasheet, September 2014, Reference Number: 330784-001.)
  // This field records whether the quirk is present.
  bool mHasRamUnitsQuirk;

  // The abovementioned 15.3 microJoules value.
  static const double kQuirkyRamJoulesPerTick;

  // The previous sample's MSR values.
  uint64_t mPrevPkgTicks;
  uint64_t mPrevPp0Ticks;
  uint64_t mPrevPp1Ticks;
  uint64_t mPrevDdrTicks;

  // The struct passed to diagCall64().
  pkg_energy_statistics_t* mPkes;

#elif defined(__linux__)

  Domain* mPkg;
  Domain* mCores;
  Domain* mGpu;
  Domain* mRam;
#endif

 public:
  RAPL();

  ~RAPL();

  static double Joules(uint64_t aTicks, double aJoulesPerTick);

  void EnergyEstimates(double& aPkg_J, double& aCores_J, double& aGpu_J, double& aRam_J);
};
