
#include "power_meter.h"

#include <string>
#include <iostream>

int main(int argc, char** argv) {

    // int nb_gpus = get_gpu_info().size();
    PowerMeter pwm(1000);

    std::string cmd;
    for(int i=1; i<argc; ++i) {
        cmd.append(argv[i]);
        cmd.append(" ");
    }
    pwm.start();
    system(cmd.c_str());
    // pwm.tick();
    pwm.stop();

    printf("RAPL: %8.2f mWh (Pavg: %6.2f W)\n", pwm.energy_mWh(), pwm.poweravg_W());
    printf("Time s: %6.3f\n", pwm.elapsed_ms()*1e-3);

    printf("\n");
}
