
#pragma once

#include "usage_info.h"
#include "rapl.h"
#include <chrono>

class PowerMeter
{
protected:

    static PowerMeter* ms_instance;
    
    RAPL rapl;
    GpuSampler gpu;

    int m_sampling_step_ms;
    unsigned long m_elapsed_ms;
    int m_nb_samples;
    double m_energy_mWh;
    double m_gpu_mWh;
    
    std::chrono::time_point<std::chrono::steady_clock> m_start;

public:

    static PowerMeter& instance() { return *ms_instance; }

    PowerMeter(int sampling_step_ms = 1000);

    ~PowerMeter();

    void start();
    void tick();
    void stop();

    unsigned long elapsed_ms() const;
    double energy_mWh() const;
    double poweravg_W() const;
    double gpu_mWh() const;
};
