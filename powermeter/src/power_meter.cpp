
#include "power_meter.h"
#include <iostream>
#include <sys/time.h>
#include <signal.h>
#include <cstring>

static void Controller_alrm_handler(int aSigNum, siginfo_t* aInfo, void* aContext);
void int_handler(int aSigNum, siginfo_t* aInfo, void* aContext);

PowerMeter::PowerMeter(int sampling_step_ms)
    : m_sampling_step_ms(sampling_step_ms), m_nb_samples(0), m_gpu_mWh(0)
{
    if(ms_instance!=0) {
        std::cerr << "ERROR, you cannot create multiple PowerMeter instances\n";
        return;
    }
    ms_instance = this;

    struct sigaction sa;
    memset(&sa, 0, sizeof(sa));
    sa.sa_flags = SA_RESTART | SA_SIGINFO;
    sigemptyset(&sa.sa_mask);
    sa.sa_sigaction = Controller_alrm_handler;
    if (sigaction(SIGALRM, &sa, NULL) < 0) {
        std::cerr << "ERROR sigaction(SIGALRM) failed\n";
        return;
    }

    // Set up the timer.
    struct itimerval timer;
    timer.it_interval.tv_sec = m_sampling_step_ms / 1000;
    timer.it_interval.tv_usec = (m_sampling_step_ms % 1000) * 1000;
    timer.it_value = timer.it_interval;
    if (setitimer(ITIMER_REAL, &timer, NULL) < 0) {
        std::cerr << "ERROR setitimer() failed\n";
        return;
    }

    // Catch ctrl+c to properly stop the samplers
    {
        struct sigaction sa;
        memset(&sa, 0, sizeof(sa));
        sa.sa_flags = SA_RESTART | SA_SIGINFO;
        sigemptyset(&sa.sa_mask);
        sa.sa_sigaction = int_handler;
        if (sigaction(SIGINT, &sa, NULL) < 0) {
            std::cerr << "ERROR sigaction(SIGINT) failed\n";
        }
    }
}

PowerMeter::~PowerMeter() {
    gpu.stop();
}

void PowerMeter::start() {
    m_start = std::chrono::steady_clock::now();
    gpu.reset();
    m_nb_samples = 0;
}

void PowerMeter::tick()
{
    gpu.accumulate_values();
    ++m_nb_samples;
}

void PowerMeter::stop()
{
    auto current = std::chrono::steady_clock::now();
    m_elapsed_ms = std::chrono::duration_cast<std::chrono::milliseconds>(current-m_start).count();

    // A special value that represents an estimate from an unsupported RAPL domain.
    double pkg_J=0, cores_J=0, gpu_J=0, ram_J=0;
    rapl.EnergyEstimates(pkg_J, cores_J, gpu_J, ram_J);
    double joules = pkg_J+ram_J;
    m_energy_mWh = joules/3600.*1e3;
    double gpu_W = gpu.averages(m_nb_samples)[0].power_W;
    if (gpu_W>0) {
        m_gpu_mWh = gpu_W*m_elapsed_ms/3600;
        m_energy_mWh += m_gpu_mWh;
    }

    gpu.stop();
}

unsigned long PowerMeter::elapsed_ms() const {
    return m_elapsed_ms;
}

double PowerMeter::energy_mWh() const {
    return m_energy_mWh;
}

double PowerMeter::gpu_mWh() const {
    return m_gpu_mWh;
}

double PowerMeter::poweravg_W() const {
    return m_energy_mWh/(double(m_elapsed_ms)/3600);
}

PowerMeter* PowerMeter::ms_instance = 0;

void Controller_alrm_handler(int aSigNum, siginfo_t* aInfo, void* aContext)
{
    PowerMeter::instance().tick();
}

void int_handler(int aSigNum, siginfo_t* aInfo, void* aContext)
{
    PowerMeter::instance().stop();
    exit(0);
}
