
#pragma once
#include <vector>
#include "sampler.h"

struct gpu_info {
  gpu_info(double c, double v, double d, double p=0)
    : core_use(c), video_use(v), device_use(d), power_W(p)
  {}
  double core_use;
  double video_use;
  double device_use;
  double power_W;
};

class GpuSampler : public AccumulatingSampler
{
  int m_nb_gpus;
public:
	
	GpuSampler();

	void accumulate_values();

  std::vector<gpu_info> averages(int nb_samples);

	~GpuSampler();

	void stop();
};

std::vector<gpu_info> get_gpu_info();

