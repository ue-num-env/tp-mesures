
#include <sstream>
#include <iostream>
#include <fstream>
#include <memory>
#include <string>
#include "usage_info.h"

#ifdef __linux__
#define LINUX_BACKEND
#endif


#ifdef LINUX_BACKEND

#include <sys/stat.h>
#include <unistd.h>

inline bool file_exists(const std::string& name)
{
  struct stat buffer;   
  return (stat (name.c_str(), &buffer) == 0); 
}

long read_long(const std::string& filename) {
  long val = -1;
  std::ifstream f(filename);
  if(f) {
    f >> val;
  }
  return val;
}

std::string read_string(const std::string& filename) {
  std::string val;
  std::ifstream f(filename);
  if(f) {
    f >> val;
  }
  return val;
}
#endif


GpuSampler::GpuSampler()
  : AccumulatingSampler(0)
{
  m_nb_gpus = get_gpu_info().size();
  this->resize(m_nb_gpus*4);
}

GpuSampler::~GpuSampler() {}

void GpuSampler::accumulate_values()
{
  auto gpus = get_gpu_info();
  for(int k=0; k<m_nb_gpus; ++k)
  {
    auto& gpu = gpus[k];
    m_values[4*k+0] += gpu.core_use;
    m_values[4*k+1] += gpu.video_use;
    m_values[4*k+2] += gpu.device_use;
    m_values[4*k+3] += gpu.power_W;
  }
}

std::vector<gpu_info> GpuSampler::averages(int nb_samples) {
  auto raw = static_cast<AccumulatingSampler*>(this)->averages(nb_samples);

  std::vector<gpu_info> res;
  for(int i=0; i<m_nb_gpus; ++i) {
    res.emplace_back(raw[4*i+0],raw[4*i+1],raw[4*i+2],raw[4*i+3]);
  }
  return res;
}

void GpuSampler::stop() {}

#if defined(LINUX_BACKEND)

#include <regex>
#include <cstdio>
#include <array>
#include <stdexcept>

std::string exec(const char* cmd)
{
    std::array<char, 128> buffer;
    std::string result;
    std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd, "r"), pclose);
    if (!pipe) {
        throw std::runtime_error("popen() failed!");
    }
    while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
        result += buffer.data();
    }
    return result;
}

std::vector<gpu_info> get_gpu_info()
{
  // try using nvidia-smi
  static bool first = true;
  static bool have_nvidia_smi = true;

  const char* cmd = "nvidia-smi -q  -d UTILIZATION,POWER";

  std::string output;
  std::vector<gpu_info> res;

  if(first) {
    if(system("nvidia-smi -L")!=0) {
      have_nvidia_smi = false;
      std::cerr << "WARNING failed to call nvidia-smi, GPU monitoring disabled\n";
    }
    first = false;
  }

  if(have_nvidia_smi) {
    try {
      output = exec(cmd);
    } catch (...) {        
      return {};
    }

    // parse output
    double percent, power;

    std::smatch m;
    {
      // "        Gpu                         : 0 %"
      std::regex e("\\s*Gpu\\s*:\\s*([0-9]*\\.*[0-9]*) %");
      if(std::regex_search (output,m,e))
        percent = std::atof(std::string(m[1]).c_str());
    }
    {
      // "        Power Draw                  : 15.51 W"
      std::regex e("\\s*Power Draw\\s*:\\s*([0-9]*\\.*[0-9]*) W");
      if(std::regex_search (output,m,e))
        power = std::atof(std::string(m[1]).c_str());
    }

    res.emplace_back(-1,-1,percent,power);
  }

  return res;
}

#elif defined(__APPLE__)


#else

std::vector<gpu_info> get_gpu_info()
{
  static bool first = true;
  if(first)
    std::cerr << "WARNING get_gpu_info not implemented yet for your system\n";
  first = false;
  return {};
}

#endif