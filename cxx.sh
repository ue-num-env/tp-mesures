#!/bin/bash

CXX="${CXX:-g++}"
FLAGS="-std=c++14  -I eigen -I powermeter/src -L powermeter/build/ "
LIBS=" -lpowermeter"
if [[ "$OSTYPE" == "darwin"* ]]; then
    FLAGS="$FLAGS -framework IOKit -framework CoreFoundation -framework Foundation"
    CXX=clang++-mp-18
    LIBS="$LIBS /opt/local/libexec/llvm-18/lib/libunwind.a"
fi
echo "Running command: '" $CXX $FLAGS $@ $LIBS \'
$CXX $FLAGS $@ $LIBS
